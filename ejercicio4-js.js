var a = [true, 5, false, 'hola ', 'adios ', 2];
document.write(a + "<br>");
if (a[3] > a[4]) { document.write('El elemento 3 es mayor que el 4<br>'); } else { document.write('El elemento 4 es mayor que el 3<br>'); }
document.write(a[1] + ' + ' + a[5] + '=' + (a[1] + a[5]) + "<br>");

document.write(a[1] + ' - ' + a[5] + '=' + (a[1] - a[5]) + "<br>");

document.write(a[1] + ' * ' + a[5] + '=' + (a[1] * a[5]) + "<br>");

document.write(a[1] + ' / ' + a[5] + '=' + (a[1] / a[5]) + "<br>");

document.write('El resto de la división de ' + a[1] + ' entre ' + a[5] + ' es ' + (a[1] % a[5]));